function fetchRandomNumbers(){
    return new Promise((resolve,reject) => {
        setTimeout(() => {
        let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
        if(randomNum) {
            resolve(randomNum)
        } else {
            reject("error in getting the random number")
        }
    },(Math.floor(Math.random() * (5)) + 1) * 1000);
})
}






function fetchRandomString(){
    return new Promise((resolve,reject) => {
        setTimeout(() => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 5; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            if(result.length <= 0) {
                reject("error in getting the string");
            } else {
                resolve(result);
            }
        },(Math.floor(Math.random() * (5)) + 1) * 1000);
    })
}








function randomNumGenerate() {
    return new Promise((resolve,reject) => {
        let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0 ;
        if(randomNum) {
            resolve(randomNum);
        }
        else {
            reject("error in getting the random  number");
        }
    })
}








function randomStrGenerate() {
    return new Promise((resolve,reject) => {
            let result           = '';
            let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for ( let i = 0; i < 4; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            if(result.length <= 0) {
                reject("error in getting the string");
            } else {
                resolve(result);
            }
    })
}













fetchRandomNumbers().then((num) => {
    console.log("the random number generated is ",num);
})
.catch((err) => {
    console.error(err);
})


fetchRandomString().then((string) => {
    console.log("the random string generated is  ",string);
})
.catch((err) => {
    console.log(err);
})

randomNumGenerate().then((num1) => {
    return num1;
}).then((num1) => {
    randomNumGenerate().then((num2) => {
        console.log(`sum of ${num1} and ${num2} is `);
        console.log(num1+num2);
    })
    .catch((err) => {
        console.error(err);
    })
})
.catch((err) => {
    console.error(err);
})


randomNumGenerate().then((num) => {
    return num ;
}).then((num) => {
    randomStrGenerate().then((str) => {
        console.log("the random generated string and number is ",num+str)
    })
    .catch((err) => {
        console.error(err);
    })
})
.catch((err) => {
    console.error(err);
})






randomNumGenerate().then((n1)=>{
    return n1;
}).then((sum) => {
    randomNumGenerate().then((n2) => {
        return sum+n2;
    })
    .then((sum) => {
        randomNumGenerate().then((n3) => {
            return sum+n3;
        })
        .then((sum) => {
            randomNumGenerate().then((n4) => {
                return sum+n4;
            })
            .then((sum) => {
                randomNumGenerate().then((n5) => {
                    return sum+n5;
                })
                .then((sum) => {
                    randomNumGenerate().then((n6) => {
                        return sum+n6;
                    })
                    .then((sum) => {
                        randomNumGenerate().then((n7) => {
                            return sum+n7;
                        })
                        .then((sum) => {
                            randomNumGenerate().then((n8) => {
                                return sum+n8;
                            })
                            .then((sum) => {
                                randomNumGenerate().then((n9) => {
                                    return sum+n9;
                                })
                                .then((sum) => {
                                    randomNumGenerate().then((n10) =>{
                                        console.log("the sum of numbers is ",(sum+n10));
                                    })
                                    .catch((err) => {
                                    console.error(err);
                                    })
                                })
                                .catch((err) => {
                                console.error(err);
                                })
                            })
                            .catch((err) => {
                            console.error(err);
                            })
                        })
                        .catch((err) => {
                        console.error(err);
                        })
                    })
                    .catch((err) => {
                    console.error(err);
                    })
                })
                .catch((err) => {
                console.error(err);
                })
            })
            .catch((err) => {
            console.error(err);
            })
        })
        .catch((err) => {
        console.error(err);
        })
    })
    .catch((err) => {
    console.error(err);
    })
})
.catch((err) => {
    console.error(err);
})




async function numGenerate() {
    const num = await fetchRandomNumbers();
    console.log(`the random generated using async-await is ${num}.`);
}
numGenerate();

async function strGenerate() {
    const str = await fetchRandomString();
    console.log(`the random String ganerated using async-await is  ${str} .`);
}
strGenerate();

async function twoRandomNumSum() {
    const number_1 = await randomNumGenerate();
    const number_2 = await randomNumGenerate();
    console.log(`the sum of two random numbers ${number_1} , ${number_2} is ${number_1+number_2} .`);
}
twoRandomNumSum();

async function strNumConcate() {
    const number = await randomNumGenerate();
    const str = await randomStrGenerate();
    console.log(`the Random String ${str} and number ${number} concatination is ${str+number} .`);
}
strNumConcate();







// fetchRandomNumbers((randomNum) => console.log(randomNum))
// fetchRandomString((randomStr) => console.log(randomStr))