function fetchData(data) {
    return new Promise(resolve => setTimeout(() => {
      resolve(data)
    }, 1000))
}

function sendUserLoginRequest(userId){
    console.log(`Sending login request for ${userId}...`);
    return fetchData(userId).then(() => {
        console.log('Login successfull.', userId);
        return userId;
    })
}

function getUserProfile(userId){
    console.log(`Fetching profile for ${userId}...`);
    return fetchData({
        user1: {name: 'Vijay', points: 100},
        user2: {name: 'Sahana', points: 200}
    }).then(profiles => {
        console.log(`Received profile for ${userId}`);
        return profiles[userId];
    });
}

function getUserPosts(userId){
    console.log(`Fetching posts for ${userId}...`);
    return fetchData({
        user1: [{id: 1, title: 'Economics 101'}, {id: 2, title: 'How to negotiate'}],
        user2: [{id: 3, title: 'CSS Animations'}, {id: 4, title: 'Understanding event loop'}]
    }).then(posts => {
        console.log(`Received posts for ${userId}`);
        return posts[userId];
    });
}


/**
 * Task 1: Send a login request for user1 -> get user profile data -> get user posts data
 */


/**
 * Task 2: Send a login request for user1 -> get user profile data and get user posts data simultaneously
 */

async function userDataParallel() {
    console.time('userData-parallel');
    try {
      const userid = await sendUserLoginRequest('user1');
      console.log(userid);
      const userProfile = await getUserProfile(userid);
      console.log(userProfile);
      const userPost = await getUserPosts(userid);
      console.log(userPost);
                
    }
    catch(err) {
      console.error("error")
    }
    console.timeEnd('userData-parallel');
  }



  async function userDataSerial(){
    let userid,userProfile,userPost;
    console.time('userData-serial');
    try {
      userid = await sendUserLoginRequest('user1');
      console.log(userid);
    }
    catch(err) {
      console.error("errors in getting the userid");
    }
    try {
      userProfile = await getUserProfile(userid);
      console.log(userProfile);
    }
    catch(err) {
      console.error("error in getting the user profile")
    }
    try {
      userPost = await getUserPosts(userid);
      console.log(userPost);
    }
    catch {
      console.error("error in getting the user posts")
    }
    console.timeEnd('userData-serial');
  }
userDataSerial();
userDataParallel();