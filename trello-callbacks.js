function getBoard(callback) {
  console.log('Fetching board...');
  return setTimeout(function() {
    let board = {
      id: "def453ed",
      name: "Thanos"
    };
    console.log('Received board');
    callback(board);
  }, 1000);
}

function getLists(boardId, callback) {
  console.log(`Fetching lists for board id ${boardId}...`);
  return setTimeout(function() {
    let lists = {
      def453ed: [
        {
          id: "qwsa221",
          name: "Mind"
        },
        {
          id: "jwkh245",
          name: "Space"
        },
        {
          id: "azxs123",
          name: "Soul"
        },
        {
          id: "cffv432",
          name: "Time"
        },
        {
          id: "ghnb768",
          name: "Power"
        },
        {
          id: "isks839",
          name: "Reality"
        }
      ]
    };
    console.log(`Received lists for board id ${boardId}`);
    callback(lists[boardId]);
  }, 1000);
}

function getCards(listId, callback) {
  console.log(`Fetching cards for list id ${listId}...`);
  return setTimeout(function() {
    let cards = {
      qwsa221: [
        {
          id: "1",
          description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
        },
        {
          id: "2",
          description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
        },
        {
          id: "3",
          description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
        }
      ],
      jwkh245: [
        {
          id: "1",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "2",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "3",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "4",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        }
      ],
      azxs123: [
        {
          id: "1",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "2",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        }
      ],
      cffv432: [
        {
          id: "1",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "2",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        }
      ],
      ghnb768: [
        {
          id: "1",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        },
        {
          id: "2",
          description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
        }
      ]
    };
    if(callback(cards[listId])) {
      console.log(`Received cards for list id ${listId}`);
    }
  },1000);
}

// Task 1 board -> lists -> cards for list qwsa221
// Task 2 board -> lists -> cards for list qwsa221 and cards for list jwkh245 simultaneously
// Task 3 board -> lists -> cards for all lists simultaneously

getBoard((board) => {
  if(typeof board === 'object') {
    getLists(board.id,(list) => {
      if(Array.isArray(list)) {
          getCards('qwsa221',(cards)=> {
            if(typeof cards === 'object')
            {
              console.log(cards);
            } else {
              console.error("error in getting details of the card");
            }
          }) 
      } else {
        console.error("error in getting the details of the leist");
      }
    })
  }
  else {
    console.error("error in getting the board");
  }
});

getBoard((board) => {
  if(typeof board === 'object') {
    //console.log("the data in the object is"+board.id)
    getLists(board.id,(list) => {
      // console.log(list[0].id);
      if(Array.isArray(list)) {
          getCards(list[0].id,(cards)=> {
            // console.log(typeof cards);
            if(typeof cards === 'object')
            {
              console.log(cards);
            } else {
              console.error("error in getting details of the card  ---->  "+list[0].id);
            }
          })
          getCards(list[1].id,(cards)=> {
            if(typeof cards === 'object')
            {
              console.log(cards);
            } else {
              console.error("error in getting details of the card  ----->  "+list[1].id);
            }
          })  
      } else {
        console.error("error in getting the details of the list ");
      }
    })
  }
  else {
    console.error("error in getting the board");
  }
});

getBoard((board) => {
  if(typeof board === 'object') {
    getLists(board.id,(list) => {
      if(Array.isArray(list)) {
        for(let index = 0;index < list.length ;index ++) {
          getCards(list[index].id,(cards) => {
            if(typeof cards === 'object') {
              console.log(cards);
              return true;
            } else {
              console.error("no card found with the name --> "+list[index].id)
              return false;
            }
          })
        }
      } else {
        console.error("error in getting the details of the leist");
      }
    })
  }
  else {
    console.error("error in getting the board");
  }
});